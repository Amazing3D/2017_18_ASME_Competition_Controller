#!/bin/bash

while true; do

	Bluetooth=$(sudo service bluetooth status | grep  "device connected")
	#echo "\"$Bluetooth\""

	if [ -z "$Bluetooth"]; then
		echo "The controller isnt connected"
	else
		echo "the controller is connected"
		sudo pigpiod
		python /home/pi/Desktop/ctestfinal.py &
		python /home/pi/Desktop/shutdown_button.py &
		break
	fi
done

