# Controller project for robot

## websites used:
- https://raspberrypi.stackexchange.com/questions/29480/how-to-use-pigpio-to-control-a-servo-motor-with-a-keyboard

- https://core-electronics.com.au/tutorials/using-usb-and-bluetooth-controllers-with-python.html

- https://gist.github.com/rdb/8864666

- https://github.com/torvalds/linux/blob/master/include/uapi/linux/input.h

- https://www.raspberrypi.org/forums/viewtopic.php?f=28&t=161458

- https://github.com/Interbotix/HROS1-Framework/tree/master/Linux/project/ps3_demo/sixpair

- https://www.piborg.org/blog/pi-zero-wifi-ps3

- https://www.raspberrypi.org/forums/viewtopic.php?t=138334

- http://abyz.me.uk/rpi/pigpio/examples.html#Python code
